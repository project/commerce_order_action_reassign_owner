CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides a new Action plugin that lets a Commerce Order be assigned 
to a new user (using Views Bulk Operations, for example).

One use case would be a situation in which a customer has multiple accounts for 
their various employees, and the orders an employee has placed still need to be 
accessible after they leave the customer's company.


More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/commerce_order_action_reassign_owner)
 
 * To submit bug reports and feature suggestions, or to track changes:
   (https://www.drupal.org/project/issues/commerce_order_action_reassign_owner)


REQUIREMENTS
------------

This module is depended on drupal commerce modules
(https://www.drupal.org/project/commerce)


INSTALLATION
------------

 * Install the Commerce Order Action Reassign Owner module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.

CONFIGURATION
-------------

 * 1. Navigate to `Administration > Extend` and enable the module.
   2. This module provides a new Action plugin that lets a Commerce 
      Order be assigned to a new user.


MAINTAINERS
-----------

Current maintainers:

 * Nigel Cunningham (NigelCunningham) - https://www.drupal.org/u/nigelcunningham
